# OutOfTheBox

## Description
Interactive audio game in German language. 

## Installation (Windows only)
- Clone the repo
- Open ScriptExecuter.exe 
- Select OOTB.zip
- Check "AUTO" Button
- Have fun! 

## Authors and acknowledgements
The ScriptExecuter.exe was developed by Prof. Manfred Hild.
The rest was developed by Patricia Waldmann and me.
This project was created during the study of humoid robotic at the University of Applied Sciences BHT.

## License
MIT license